const fs = require("fs");
const path = require("path");

// Function to create a directory of random JSON files
function createRandomJsonFiles(directoryPath, numberOfFiles, callback) {
  fs.mkdir(directoryPath, (err) => {
    if (err) {
      return callback(err);
    }

    let createdFilesCount = 0;
    const filePaths = [];

    for (let files = 0; files < numberOfFiles; files++) {
      const randomData = { randomKey: Math.random() };
      const filePath = path.join(directoryPath, `file${files}.json`);
      const fileContent = JSON.stringify(randomData);

      fs.writeFile(filePath, fileContent, (err) => {
        if (err) {
          return callback(err);
        }

        filePaths.push(filePath);
        createdFilesCount++;

        if (createdFilesCount === numberOfFiles) {
          callback(null, filePaths);
        }
      });
    }
  });
}

// Function to delete files simultaneously
function deleteFilesSimultaneously(filePaths, callback) {
  let deletedFiles = 0;

  let filesDeleted = [];

  // unlinks or permanent delete files in Randomjson
  function deleteNextFiles(filepath, index) {
    // base condition
    if (index === filepath.length) {
      return;
    }

    // Pushing fs unlink promises to unlinkfilepromises
    fs.unlink(filepath[index], (err) => {
      if (err) {
        callback(err);
      }

      filesDeleted.push(filepath[index]);
      deletedFiles++;

      if (deletedFiles == filePaths.length) {
        callback(null, filesDeleted);
      }
    });

    deleteNextFiles(filepath, index + 1);
  }

  deleteNextFiles(filePaths, 0);
}
// Main function to coordinate the entire process
function fsProblem1(directoryPath, numberOfFiles) {
  createRandomJsonFiles(directoryPath, numberOfFiles, (err, filePaths) => {
    if (err) {
      console.error("Error creating random JSON files:", err);
    } else {
      console.log("Random JSON files created:", filePaths);
      deleteFilesSimultaneously(filePaths, (err, deletedFilePaths) => {
        if (err) {
          console.error("Error deleting files:", err);
        } else {
          console.log("Files deleted successfully:", deletedFilePaths);
        }
      });
    }
  });
}

module.exports = fsProblem1;
